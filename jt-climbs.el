;;; jt-climbs.el --- Personal climbs for GNU Emacs  -*- lexical-binding: t -*-

;; Copyright (c) 2022-2023 John Task <q01@disroot.org>

;; Author: John Task <q01@disroot.org>
;; Version: 2.0.0
;; Package-Requires: ((emacs "28.1"))
;; URL: https://gitlab.com/q01_code/emacs-dotfiles

;; This file is not part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; My personal climbs for GNU Emacs.
;;
;; See <https://gitlab.com/q01_code/climber>

;;; Code:

(require 'climber)

;; wget
(climber-def wget
  :title "GNU Wget"
  :bind  "C-c s w"
  :args
  (("u" "url" "" url-multi)
   ("o" "write to file" "--output-document=" file)
   ("i" "download from file" "--input-file=" file)
   ("c" "continue a download" "--continue")
   ("h" "use only https" "--https-only" toggle-default)
   ("t" "number of tries" "--tries=" natnum)
   ("T" "timeout in seconds" "--timeout=" natnum)))

;; mpv
(climber-def mpv
  :bind "C-c s v"
  :args
  (("o" "open file" "" file)
   ("f" "full screen" "--fullscreen")))

;; git clone
;;; Although `magit-clone' exists, this is faster
(climber-def git-clone
  :run  "git clone"
  :bind "C-c s g"
  :args
  (("u" "url" "" url)
   ("o" "save on directory" "" dir)
   ("d" "depth" "--depth " natnum)
   ("r" "recursive" "--recursive")))


(provide 'jt-climbs)
