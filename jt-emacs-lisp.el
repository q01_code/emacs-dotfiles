;;; jt-emacs-lisp.el --- Custom functions for Emacs -*- lexical-binding: t -*-

;; Copyright (C) 2022-2023 John Task <q01@disroot.org>

;; Author: John Task <q01@disroot.org>
;; Version: 2.0.0
;; Package-Requires: ((emacs "28.1"))
;; URL: https://gitlab.com/q01_code/emacs-dotfiles

;; This file is not part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Personal functions for GNU Emacs.
;;
;; I didn't write all of these snippets.  I give an URL to the source
;; when I'm not the author.  Of course, the code from source won't
;; always match what is here, as I take the freedom to modify the
;; code.

;;; Code:

(require 'cl-lib)
(require 'subr-x)

(defvar jt-emacs-load-path (locate-user-emacs-file "lisp-dir/")
  "Path where custom lisp code is stored.")

(defvar jt-prompt-title "Welcome to Emacs!"
  "Message shown at startup.")

(defvar jt-org-pretty-p nil
  "Non-nil if `jt-org-prettify-buffer' is in effect.")

(defvar-local hide-cursor--original nil)

;; Silence compiler warnings
(declare-function outline-show-children nil)
(declare-function outline-show-entry nil)
(declare-function outline-on-heading-p nil)
(declare-function org-fold-show-children nil)
(declare-function org-fold-show-entry nil)
(declare-function org-back-to-heading nil)
(declare-function org-in-src-block-p nil)
(declare-function org-at-block-p nil)
(declare-function org-at-property-drawer-p nil)
(declare-function org-at-property-p nil)
(declare-function word-at-point nil)
(declare-function ring-ref nil)
(declare-function eshell/pwd nil)
(declare-function cape-dict nil)
(declare-function corfu-mode nil)
(declare-function jinx-mode nil)
(declare-function org-element-table-cell-parser nil)
(declare-function org-at-table-p nil)
(declare-function org-insert-item nil)
(declare-function org-in-item-p nil)
(declare-function org-insert-todo-heading nil)
(declare-function org-at-item-checkbox-p nil)
(declare-function org-entry-end-position nil)
(declare-function org-entry-beginning-position nil)
(declare-function org-at-heading-p nil)
(declare-function org-element-context nil)
(declare-function org-return nil)
(declare-function org-element-property nil)
(declare-function org-bullets-mode nil)
(declare-function embark-act nil)
(declare-function avy-goto-char-timer nil)
(declare-function god-local-mode nil)
;; NOTE: This is bad practice
(defvar kmacro-call-repeat-key t)
(defvar nov-metadata nil)
(defvar avy-ring (make-ring 20))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System dependent setup ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Ensure backward compatibility by loading compat.el
(let ((load-path
       (append
        (ensure-list (concat jt-emacs-load-path "compat")) load-path)))
  (require 'compat))

;; My Android device is named 'localhost'. There's no other way of
;; knowing it's Android, as I use Emacs inside an Alpine container
;; there.
(defvar jt--is-android (equal (system-name) "localhost")
  "Non-nil if current device is Android.")

;; 'setopt' is not available on my Android for now, and is not
;; provided by compat either.
(when (version< emacs-version "29.1")
  (defmacro setopt (variable value)
    "Set VARIABLE to VALUE, and return VALUE."
    `(customize-set-variable ',variable ,value)))

;;;;;;;;;;;;;;;;;
;; For init.el ;;
;;;;;;;;;;;;;;;;;

(defgroup jt-emacs nil
  "User options for my dotemacs."
  :group 'file)

(defmacro jt-with-emacs-home-in-path (&rest body)
  "Eval BODY with `user-emacs-directory' in the load-path."
  (declare (indent defun))
  `(let ((load-path (append (ensure-list user-emacs-directory) load-path)))
     ,@body))

;; Borrowed from Doom, but simplified
;;; https://github.com/doomemacs/doomemacs

(defun jt--setq-hook-title (hook var)
  "Return a name for a function on HOOK modifying VAR."
  (list (intern (concat "jt--" (string-remove-suffix "hook" (symbol-name hook))
                        "set-" (symbol-name var)))))

(defmacro jt-setq-hook (hook var value)
  "Sets buffer-local VAR to VALUE on HOOK."
  (declare (indent 1))
  (macroexp-progn
   (cl-loop for title in (jt--setq-hook-title hook var)
            collect `(defun ,title (&rest _)
                       ,(format "%s = %s" value (pp-to-string value))
                       (setq-local ,var ,value))
            collect `(add-hook ',hook #',title -90))))


;;;;;;;;;;;;;;;;;;;;;
;; For use-package ;;
;;;;;;;;;;;;;;;;;;;;;

;; This use-package keyword will add the directory on
;; `jt-emacs-load-path' that matches the name of the package (or any
;; string provided) to the load path

(defun use-package-normalize/:local (name-symbol keyword args)
  (use-package-only-one (symbol-name keyword) args
    (lambda (label arg)
      (cond
       ((symbolp arg) arg)
       ((stringp arg) arg)
       (t
        (use-package-error ":local argument must be a string or symbol"))))))

(defun use-package-handler/:local (name-symbol keyword arg rest state)
  (let ((body (use-package-process-keywords name-symbol rest state)))
    (use-package-concat
     (cond ((stringp arg)
            (list `(add-to-list 'load-path ,(concat jt-emacs-load-path arg))))
           ((not (null arg))
            (let ((dir
                   (concat jt-emacs-load-path (symbol-name name-symbol))))
              (when (file-exists-p dir)
                (list `(add-to-list 'load-path ,dir))))))
     body)))


;;;;;;;;;;;;;;;;;;;;;
;; smart-open-line ;;
;;;;;;;;;;;;;;;;;;;;;

(defun jt-smart-open-line (n)
  "Insert a newline at the end of current line, without moving point.
With positive argument N, insert N newlines.
If N is negative, insert N newlines above current line.
When called with a plain prefix argument, delete blank lines around point.
With a double prefix argument, just delete blank lines above point."
  (interactive "P")
  (let ((arg (prefix-numeric-value n)))
    (save-excursion
      (cond
       ((or (equal n '(4)) (equal n '(16)))
        (beginning-of-line)
        (ensure-empty-lines 0)
        (and (= arg 4) (delete-blank-lines)))
       ((> arg 0)
        (end-of-line)
        (newline arg))
       (t
        (beginning-of-line)
        (newline (- 0 arg)))))))


;;;;;;;;;;;;;;;;;;;;;;;;
;; open-line-and-move ;;
;;;;;;;;;;;;;;;;;;;;;;;;

;; https://github.com/bbatsov/crux

(defun jt-open-line-and-move (arg)
  "Insert an empty line after the current line.
Position the cursor at its beginning, according to the current mode.

With a prefix ARG open line above the current line."
  (interactive "P")
  (cond (arg
         (move-beginning-of-line nil)
         (insert "\n")
         ;; crux checks for `electric-indent-inhibit'.
         ;; I don't feel the need for now.
         (forward-line -1)
         (indent-according-to-mode))
        (t
         (move-end-of-line nil)
         (newline-and-indent))))


;;;;;;;;;;;;;;;
;; open-with ;;
;;;;;;;;;;;;;;;

;; https://github.com/bbatsov/crux

(defun jt-open-with (arg)
  "Open visited file in default external program.
When in dired mode, open file under the cursor.

With a prefix ARG always prompt for command to use."
  (interactive "P")
  (let* ((current-file-name
          (if (derived-mode-p 'dired-mode)
              (dired-get-file-for-visit)
            buffer-file-name))
         (open (pcase system-type
                 (`darwin "open")
                 ((or `gnu `gnu/linux `gnu/kfreebsd) "xdg-open")))
         (program (if (or arg (not open))
                      (read-shell-command "Open current file with: ")
                    open)))
    (call-process program nil 0 nil current-file-name)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; increment-number-at-point ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://www.emacswiki.org/emacs/IncrementNumber

(defun jt-increment-number-at-point (&optional n)
  "Increment number at point by N.
With no argument, increment by one.
Negative argument decrements number at point accordingly."
  (interactive "p")
  (skip-chars-backward "0-9")
  (or (looking-at "[0-9]+")
      (error "No number at point"))
  (replace-match (number-to-string (+ n (string-to-number (match-string 0))))))

(defalias 'increment-number-at-point #'jt-increment-number-at-point)


;;;;;;;;;;;;;;;;;;;;;;;;;
;; kill-current-buffer ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

(defun jt-kill-current-buffer ()
  "Kill current buffer."
  (interactive)
  (kill-buffer (current-buffer)))

(defalias 'kill-current-buffer #'jt-kill-current-buffer)


;;;;;;;;;;;;;;;;;;;;;;;
;; copy-url-at-point ;;
;;;;;;;;;;;;;;;;;;;;;;;

(defun jt-copy-url-at-point ()
  "Copy the URL at point. If there's no URL, copy the first URL in the line."
  (interactive)
  (let ((url (thing-at-point 'url)))
    (save-excursion
      (cond (url
             (kill-new url)
             (message "Copied \"%s\"" url))
            (t
             (beginning-of-line)
             (if (re-search-forward "http[s]?://" (pos-eol) t)
                 (progn
                   (setq url (thing-at-point 'url))
                   (kill-new url)
                   (message "Copied \"%s\"" url))
               (user-error "No URL")))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; activate-mark-interactive ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun jt-activate-mark-interactive ()
  "Activate the mark."
  (interactive)
  (activate-mark))


;;;;;;;;;;;;;;;
;; mark-dwim ;;
;;;;;;;;;;;;;;;

(defun jt-mark-dwim (&optional arg)
  "Mark symbol at point, and keep expanding by s-expressions.

Current symbol is marked if (and only if) region is not already
active.

If no symbol at point, just call `mark-sexp'.

If no more s-expressions can be marked, call `er/expand-region'
instead.

This command exist to address perceived inefficiencies of vanilla
expand-region, such as having to calculate what to mark next when
almost always I desire to mark a symbol.  Also, this command
unifies stock mark-sexp and expand-region with almost no
drawbacks.

Region will be activated after the execution of this command,
regardless of `transient-mark-mode' original value."
  (interactive "P")
  (cond ((or arg (region-active-p))
         (condition-case nil (call-interactively #'mark-sexp)
           (t (call-interactively #'er/expand-region))))
        ((symbol-at-point)
         (er/mark-symbol))
        ((eq (point) (cdr (bounds-of-thing-at-point 'sexp)))
         (mark-sexp -1 t))
        (t
         (call-interactively #'mark-sexp)))
  (unless (region-active-p)
    (activate-mark)))


;;;;;;;;;;;;;;;;;;;;;;;;;
;; show-welcome-buffer ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://xenodium.com/emacs-a-welcoming-experiment/

(defun jt-show-welcome-buffer ()
  "Show *Welcome* buffer."
  (when (display-graphic-p)
    (with-current-buffer (get-buffer-create "*Welcome*")
      (setq truncate-lines t)
      (let* ((buffer-read-only)
             (image-path "~/.emacs.d/emacs.png")
             (image (create-image image-path))
             (size (image-size image))
             (height (cdr size))
             (width (car size))
             (top-margin (floor (/ (- (window-height) height) 2)))
             (left-margin (floor (/ (- (window-width) width) 2))))
        (erase-buffer)
        (setq mode-line-format nil)
        (goto-char (point-min))
        (insert (make-string top-margin ?\n ))
        (insert (make-string left-margin ?\ ))
        (insert-image image)
        (insert "\n\n\n")
        (insert (make-string (floor (/ (- (window-width) (string-width jt-prompt-title)) 2)) ?\ ))
        (insert jt-prompt-title))
      (setq cursor-type nil)
      (read-only-mode)
      (switch-to-buffer (current-buffer))
      (local-set-key (kbd "q") 'kill-this-buffer))))


;;;;;;;;;;;;;;;;;;;;;;
;; new-empty-buffer ;;
;;;;;;;;;;;;;;;;;;;;;;

;; Based on http://xahlee.info/emacs/emacs/emacs_new_empty_buffer.html

(defun jt-new-empty-buffer ()
  "Create a new empty buffer.
New buffer will be named “temporal” or “temporal<2>”, “temporal<3>”, etc."
  (interactive)
  (let ((buf (generate-new-buffer "temporal")))
    (switch-to-buffer buf)
    (org-mode)
    (setq buffer-offer-save t)))


;;;;;;;;;;;;;;;;;;;;;;
;; space-to-newline ;;
;;;;;;;;;;;;;;;;;;;;;;

;; Based on http://xahlee.info/emacs/emacs/emacs_space_to_newline.html

(defun jt-space-to-newline ()
  "Replace space sequence on region to a newline char."
  (interactive)
  (save-excursion
    (save-restriction
      (narrow-to-region (point) (mark))
      (goto-char (point-min))
      (while (re-search-forward " +" nil t)
        (replace-match "\n")))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; recompile-emacs-directory ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun jt-recompile-emacs-directory ()
  "Recompile all updated Elisp files on user directory."
  (byte-recompile-directory user-emacs-directory))


;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple-other-window ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

(defun jt-simple-other-window ()
  "Unconditionally select window returned by `next-window'."
  (select-window (next-window)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; clean-whitespace-region ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun jt-clean-whitespace-region (&optional arg)
  "Clean blank lines in region.
With ARG, also clean trailing whitespace."
  (interactive "P")
  (save-excursion
    (save-restriction
      (narrow-to-region (point) (mark))
      (goto-char (point-min))
      (and arg (delete-trailing-whitespace))
      (while (re-search-forward "^\n" nil t)
        (delete-region (match-beginning 0) (match-end 0))))))


;;;;;;;;;;;;;;;;;;;;
;; truncate-lines ;;
;;;;;;;;;;;;;;;;;;;;

(defun jt-truncate-lines ()
  "Truncate lines on current buffer."
  (jt-word-wrap-mode -1)
  (setq truncate-lines t))


;;;;;;;;;;;;;;;;;;;;
;; hide-mode-line ;;
;;;;;;;;;;;;;;;;;;;;

(defun jt-hide-mode-line ()
  "Hide mode line in current buffer."
  (setq mode-line-format nil))


;;;;;;;;;;;;;;;;;;;;;
;; org-return-dwim ;;
;;;;;;;;;;;;;;;;;;;;;

;; https://github.com/alphapapa/unpackaged.el

(defun jt-org-element-descendant-of (type element)
  "Return non-nil if ELEMENT is a descendant of TYPE.
TYPE should be an element type, like `item' or `paragraph'.
ELEMENT should be a list like that returned by `org-element-context'."
  ;; MAYBE: Use `org-element-lineage'.
  (when-let* ((parent (org-element-property :parent element)))
    (or (eq type (car parent)))))

(defun jt-org-return-dwim (&optional default)
  "A helpful replacement for `org-return'.  With prefix, call `org-return'.

In lists, insert a new item or end the list, with checkbox if
appropriate. In tables, insert a new row or end the table."
  ;; Inspired by John Kitchin
  ;; <kitchingroup.cheme.cmu.edu/blog/2017/04/09/A-better-return-in-org-mode/>
  (interactive "P")
  (if default
      (org-return)
    (cond
     ;; Open links like usual, unless point is at the end of a line.
     ;; And if at beginning of line, just press enter.
     ((or (and (eq 'link (car (org-element-context))) (not (eolp)))
          (eq 'subscript (car (org-element-context)))
          (bolp))
      (org-return))

     ((org-at-heading-p t)
      ;; Heading: Insert two newlines below
      (end-of-line)
      (insert "\n\n"))

     ((org-at-item-checkbox-p)
      ;; Checkbox: Insert new item with checkbox.
      (org-insert-todo-heading nil))

     ((org-in-item-p)
      ;; Plain list.  Yes, this gets a little complicated...
      (let ((context (org-element-context)))
        (if (or (eq 'plain-list (car context))  ; First item in list
                (and (eq 'item (car context))
                     (not (eq (org-element-property :contents-begin context)
                              (org-element-property :contents-end context))))
                ;; Element in list item, e.g. a link
                (jt-org-element-descendant-of 'item context))  
            ;; Non-empty item: Add new item.
            (org-insert-item)
          ;; Empty item: Close the list.
          ;; TODO: Do this with org functions rather than operating on the text.
          ;; Can't seem to find the right function.
          (delete-region (line-beginning-position) (line-end-position))
          (insert "\n"))))

     ((when (fboundp 'org-inlinetask-in-task-p)
        (org-inlinetask-in-task-p))
      ;; Inline task: Don't insert a new heading.
      (org-return))

     ((org-at-table-p)
      (cond ((save-excursion
               (beginning-of-line)
               ;; See `org-table-next-field'.
               (cl-loop with end = (line-end-position)
                        for cell = (org-element-table-cell-parser)
                        always (equal
                                (org-element-property :contents-begin cell)
                                (org-element-property :contents-end cell))
                        while (re-search-forward "|" end t)))
             ;; Empty row: end the table.
             (delete-region (line-beginning-position) (line-end-position))
             (org-return))
            (t
             ;; Non-empty row: call `org-return'.
             (org-return))))
     (t
      ;; All other cases: call `org-return'.
      (org-return)))))

(defalias 'org-return-dwim #'jt-org-return-dwim)


;;;;;;;;;;;;;;;;;;;;;;
;; Hide cursor mode ;;
;;;;;;;;;;;;;;;;;;;;;;

;; https://karthinks.com/software/more-less-emacs/

(define-minor-mode jt-hide-cursor-mode
  "Hide or show the cursor.
When the cursor is hidden `scroll-lock-mode' is enabled, so that
the buffer works like a pager."
  :global nil
  :lighter "H"
  (if jt-hide-cursor-mode
      (progn
        (scroll-lock-mode 1)
        (setq-local hide-cursor--original cursor-type)
        (setq-local cursor-type nil))
    (scroll-lock-mode -1)
    (setq-local cursor-type (or hide-cursor--original t))))

(defalias 'hide-cursor-mode #'jt-hide-cursor-mode)


;;;;;;;;;;;;;;;;;;;;
;; Windmove DWIM ;;;
;;;;;;;;;;;;;;;;;;;;

(defun jt-windmove-left-dwim ()
    "Select the window to the left of the current one.
If no window exists, then move to previous buffer."
    (interactive)
    (let ((original-window (selected-window))
          (original-position (point)))
        (other-window 1)
        (if (eq (selected-window) original-window)
            (previous-buffer)
          (progn
            (select-window original-window)
            (goto-char original-position)
            (windmove-left)))))

(defun jt-windmove-right-dwim ()
    "Select the window to the right of the current one.
If no window exists, then move to next buffer."
    (interactive)
    (let ((original-window (selected-window))
          (original-position (point)))
        (other-window 1)
        (if (eq (selected-window) original-window)
            (next-buffer)
          (progn
            (select-window original-window)
            (goto-char original-position)
            (windmove-right)))))


;;;;;;;;;;;;;;;;;;;;
;; word-wrap-mode ;;
;;;;;;;;;;;;;;;;;;;;

(define-minor-mode jt-word-wrap-mode
  "Toggle word-wrap in current buffer.

When Word Wrap mode is enabled, `word-wrap' is turned on in
this buffer.  This is thus similar to `visual-line-mode', but no
commands are redefined to act on visual lines.

Turning on this mode disables line truncation set up by
variables `truncate-lines' and `truncate-partial-width-windows'."
  :group 'visual-line
  :lighter " Wrap"
  (if jt-word-wrap-mode
      (progn
        (unless visual-line--saved-state
	  (setq-local visual-line--saved-state (list nil))
	  ;; Save the local values of some variables, to be restored if
	  ;; jt-word-wrap-mode is turned off.
	  (dolist (var '(line-move-visual truncate-lines
		                          truncate-partial-width-windows
		                          word-wrap fringe-indicator-alist))
	    (if (local-variable-p var)
	        (push (cons var (symbol-value var))
		      visual-line--saved-state))))
        (setq-local line-move-visual t)
        (setq-local truncate-partial-width-windows nil)
	(setq truncate-lines nil
	      word-wrap t
	      fringe-indicator-alist
	      (cons (cons 'continuation visual-line-fringe-indicators)
		    fringe-indicator-alist)))
    (kill-local-variable 'line-move-visual)
    (kill-local-variable 'word-wrap)
    (kill-local-variable 'truncate-lines)
    (kill-local-variable 'truncate-partial-width-windows)
    (kill-local-variable 'fringe-indicator-alist)
    (dolist (saved visual-line--saved-state)
      (when (car saved)
        (set (make-local-variable (car saved)) (cdr saved))))
    (kill-local-variable 'visual-line--saved-state)))

(defun jt-turn-on-word-wrap-mode ()
  (jt-word-wrap-mode 1))

(define-globalized-minor-mode jt-global-word-wrap-mode
  jt-word-wrap-mode jt-turn-on-word-wrap-mode)


;;;;;;;;;;;;;;;;;;;;
;; open-init-file ;;
;;;;;;;;;;;;;;;;;;;;

(defun jt-open-init-file ()
  "Open Emacs' init file."
  (interactive)
  (find-file (locate-user-emacs-file "init.el")))


;;;;;;;;;;;;;;;;;
;; writer-mode ;;
;;;;;;;;;;;;;;;;;

(define-minor-mode jt-writer-mode
  "Minor mode intended to emulate features from a word processor."
  :after-hook
  (if jt-writer-mode
      (progn
        (setq-local left-margin-width 14)
        (setq-local right-margin-width 14)
        (set-window-buffer nil (current-buffer))
        (variable-pitch-mode)
        (jinx-mode)
        (corfu-mode)
        (add-to-list 'completion-at-point-functions #'cape-dict)
        (if (eq major-mode 'org-mode)
            (setq-local org-tab-before-tab-emulation-hook #'completion-at-point)
          (keymap-local-set "TAB" #'completion-at-point))
        (setq-local line-spacing 8))
    (progn
      (setq-local left-margin-width 0)
      (setq-local right-margin-width 0)
      (set-window-buffer nil (current-buffer))
      (variable-pitch-mode -1)
      (jinx-mode -1)
      (corfu-mode -1)
      (setq-local line-spacing nil))))

(defalias 'writer-mode #'jt-writer-mode)


;;;;;;;;;;;;;;;;;
;; reader-mode ;;
;;;;;;;;;;;;;;;;;

;; This takes ideas (and code) from Protesilaos and Ric Lister

(defface jt-reader-level-1 '((t :inherit org-level-1 :height 1.30))
  "Face used for level 1 headlines."
  :group 'org-faces)

(defface jt-reader-level-2 '((t :inherit org-level-2 :height 1.20))
  "Face used for level 2 headlines."
  :group 'org-faces)

(defface jt-reader-level-3 '((t :inherit org-level-3 :height 1.10))
  "Face used for level 3 headlines."
  :group 'org-faces)

(defvar-local jt-reader-cookie nil
  "List of cookies used for `jt-reader-mode'.")

(defvar-local jt-reader-overlays-list nil
  "Overlays used for `jt-reader-mode'.")

(defun jt-reader-add-overlay (beginning end)
  "Create a single overlay from BEGINNING to END and remember it."
  (let ((overlay (make-overlay beginning end)))
    (push overlay jt-reader-overlays-list)
    (overlay-put overlay 'invisible t)))

(defun jt-reader-hide-stars ()
  "Hide stars from headings on current buffer."
  (goto-char (point-min))
  (while (re-search-forward "^\\(*+\\)" nil t)
    (jt-reader-add-overlay (match-beginning 1) (match-end 1))))

(defun jt-reader-rm-overlays ()
  "Remove overlays for this mode."
  (mapc #'delete-overlay jt-reader-overlays-list)
  (remove-from-invisibility-spec '(jt-reader-mode)))

(define-minor-mode jt-reader-mode
  "Like `writer-mode', but for reading only."
  :after-hook
  (if jt-reader-mode
      (progn
        (setq-local left-margin-width 14)
        (setq-local right-margin-width 14)
        (setq-local line-spacing 8)
        (when (eq major-mode 'org-mode)
          (setq jt-reader-cookie
                (list
                 (face-remap-add-relative 'org-level-1 'jt-reader-level-1)
                 (face-remap-add-relative 'org-level-2 'jt-reader-level-2)
                 (face-remap-add-relative 'org-level-3 'jt-reader-level-3)))
          (jt-reader-hide-stars))
        (set-window-buffer nil (current-buffer))
        (variable-pitch-mode)
        (view-mode)
        (hide-cursor-mode)
        ;; Not useful IMO, but debatable
        (set-buffer-modified-p nil))
    (setq-local left-margin-width 0)
    (setq-local right-margin-width 0)
    (setq-local line-spacing nil)
    (when (eq major-mode 'org-mode)
      (dolist (cook jt-reader-cookie)
        (face-remap-remove-relative cook))
      (jt-reader-rm-overlays))
    (set-window-buffer nil (current-buffer))
    (variable-pitch-mode -1)
    (view-mode -1)
    (hide-cursor-mode -1)))

(defalias 'reader-mode #'jt-reader-mode)


;;;;;;;;;;;;;;;;;
;; For Isearch ;;
;;;;;;;;;;;;;;;;;

;; From https://protesilaos.com/emacs/dotemacs

(defun jt-search-isearch-other-end ()
  "End current search in the opposite side of the match.
Particularly useful when the match does not fall within the
confines of word boundaries (e.g. multiple words)."
  (interactive)
  (isearch-done)
  (when isearch-other-end
    (goto-char isearch-other-end)))

(defun jt-search-isearch-abort-dwim ()
  "Delete failed `isearch' input, single char, or cancel search.

This is a modified variant of `isearch-abort' that allows us to
perform the following, based on the specifics of the case: (i)
delete the entirety of a non-matching part, when present; (ii)
delete a single character, when possible; (iii) exit current
search if no character is present and go back to point where the
search started."
  (interactive)
  (if (eq (length isearch-string) 0)
      (isearch-cancel)
    (isearch-del-char)
    (while (or (not isearch-success) isearch-error)
      (isearch-pop-state)))
  (isearch-update))


;;;;;;;;;;;;;;;;
;; For eshell ;;
;;;;;;;;;;;;;;;;

;; This is not mine; I can't recall where is it from.

(defun jt-default-prompt-function ()
  "A prompt for eshell of the form
   ┌─[$USER@$HOST] [$PWD]
   └─"
  (let ((head-face '(:foreground "#315b00"))
        (pwd (abbreviate-file-name (eshell/pwd))))
      (concat (propertize "┌─" 'face head-face)
              (user-login-name)
              "@"
              (system-name)
              " ["
              (propertize pwd 'face '((:foreground "#81d4fa")))
              "]"
              "\n"
              (propertize "└─" 'face head-face)
              (if (zerop (user-uid)) "#" "$")
              (propertize " " 'face '(:weight bold)))))


;;;;;;;;;;;;;;;;
;; For nov.el ;;
;;;;;;;;;;;;;;;;

(defun jt-nov-window-configuration-change-hook ()
  (jt-nov-post-html-render-hook)
  (remove-hook 'window-configuration-change-hook
               'jt-nov-window-configuration-change-hook
               t))

(defun jt-nov-post-html-render-hook ()
  (let ((inhibit-read-only t))
    (setq line-spacing 2)
    (hide-cursor-mode)
    (if (get-buffer-window)
        (progn (put-text-property
                1 (point-max)
                'line-prefix "   ")
               (put-text-property
                1 (point-max)
                'wrap-prefix " "))
      (add-hook 'window-configuration-change-hook
                'jt-nov-window-configuration-change-hook
                nil t))
    (setq-local next-screen-context-lines 0)
    (let ((title (cdr (assq 'title nov-metadata))))
      (rename-buffer title))
    (set-buffer-modified-p nil)))

(defun jt-nov-backward-paragraph ()
  "Scroll down until showing a whole paragraph at the top of screen.
When there's already a whole paragraph there, scroll until showing a new one."
  (interactive)
  (move-to-window-line 0)
  (backward-paragraph)
  (recenter 0)
  (save-excursion
    (forward-line)
    (beginning-of-line)
    (pulse-momentary-highlight-region (point) (+ (point) 1))))

(defun jt-nov-forward-paragraph ()
  "Scroll up until the paragraph at the top of screen is hidden."
  (interactive)
  (move-to-window-line 0)
  (forward-paragraph)
  (recenter 0)
  (save-excursion
    (forward-line)
    (beginning-of-line)
    (pulse-momentary-highlight-region (point) (+ (point) 1))))


;;;;;;;;;;;;;
;; For avy ;;
;;;;;;;;;;;;;

(defun jt-avy-action-forward-sexp (pt)
  (goto-char pt)
  (forward-sexp))

(defun jt-avy-embark ()
  "Combine avy and embark."
  (interactive)
  (call-interactively #'avy-goto-char-2)
  (call-interactively #'embark-act))

(defun jt-avy-define ()
  "Combine avy and lexic."
  (interactive)
  (call-interactively #'avy-goto-word-1)
  (call-interactively #'lexic-search-word-at-point))


;;;;;;;;;;;;;
;; For Org ;;
;;;;;;;;;;;;;

(defun jt-org-prettify-buffer ()
  "Prettify Org buffer, e.g. by hiding emphasis markers.
When already prettified, unprettify (uglify?) instead."
  (interactive)
  (cond ((not jt-org-pretty-p)
         ;; Hide emphasis markers
         (setq-local org-hide-emphasis-markers t)
         ;; Prettify symbols and code blocks
         (setq prettify-symbols-alist
               (mapcan (lambda (x) (list x (cons (upcase (car x)) (cdr x))))
                       '(("#+begin_src" . ?↪)
                         ("#+end_src" . "")
                         ("#+begin_quote" . "")
                         ("#+end_quote" . ""))))
         (prettify-symbols-mode)
         ;; Prettify headings
         (require 'org-bullets)
         (org-bullets-mode)
         ;; Set variable
         (setq-local jt-org-pretty-p t))
        (t
         ;; Show emphasis markers
         (setq-local org-hide-emphasis-markers nil)
         ;; Restore symbols
         (prettify-symbols-mode -1)
         ;; Restore headings
         (org-bullets-mode -1)
         ;; Unset variable
         (setq-local jt-org-pretty-p nil)))
  ;; Update font lock
  (font-lock-update))

(defun jt-org-make-bold ()
  "Make text bold."
  (interactive)
  (insert-pair 0 ?* ?*))

(defun jt-org-make-cursive ()
  "Make text cursive."
  (interactive)
  (insert-pair 0 ?/ ?/))

(defun jt-org-make-underline ()
  "Make text underlined."
  (interactive)
  (insert-pair 0 ?_ ?_))

(defun jt-org-back-to-heading ()
  "Go back to heading if needed."
  (unless (or (org-at-property-p) (org-at-property-drawer-p)
              (org-at-block-p) (org-in-src-block-p))
    (org-back-to-heading)))


;;;;;;;;;;;;;;;
;; For pulse ;;
;;;;;;;;;;;;;;;

(defun jt-pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))


;;;;;;;;;;;;;;;;;
;; For consult ;;
;;;;;;;;;;;;;;;;;

(defun jt-reveal-entry ()
  "Reveal Org or Outline entry."
  (cond
   ((and (eq major-mode 'org-mode)
         (org-at-heading-p))
    (org-fold-show-entry)
    (org-with-limited-levels (org-fold-show-children))
    (beginning-of-line)
    (message ""))
   ((and (or (eq major-mode 'outline-mode)
             (bound-and-true-p outline-minor-mode))
         (outline-on-heading-p))
    (outline-show-entry)
    (outline-show-children)
    (message ""))))

(defun jt-put-line-at-top ()
  "Put current line at the top of screen."
  (recenter 0))

(defun jt-consult-roam ()
  "Search all org-roam files using `consult-grep'."
  (interactive)
  (consult-grep org-roam-directory))


;;;;;;;;;;;;;;;
;; For dired ;;
;;;;;;;;;;;;;;;

;; Based on http://xahlee.info/emacs/emacs/dired_sort.html
(defun jt-dired-sort ()
  "Sort dired dir listing in different ways.
Prompt for a choice."
  (interactive)
  (let (sort-by arg)
    (setq sort-by (completing-read "Sort by: " '("date" "size" "name" "reset")))
    (cond
     ((equal sort-by "name") (setq arg "-Al "))
     ((equal sort-by "date") (setq arg "-Al -t"))
     ((equal sort-by "size") (setq arg "-Al -S"))
     ((equal sort-by "reset") (setq arg dired-listing-switches))
     (t (user-error "logic error 09535" )))
    (dired-sort-other arg)))

(defun jt-dired-ediff ()
  "Compare two marked files using `ediff'."
  (interactive)
  (let ((marked (dired-get-marked-files)))
    (ediff (car marked) (cadr marked))))


;;;;;;;;;;;;;;;;;
;; For vertico ;;
;;;;;;;;;;;;;;;;;

(defun jt-vertico-go-to-home ()
  "Delete current minibuffer input and replace it with home location."
  ;; This is here because, on my keyboard, typing the ~/ is
  ;; suprisingly annoying.
  (interactive)
  (delete-minibuffer-contents)
  (insert "~/"))


;;;;;;;;;;;;;;;;;;
;; For org-roam ;;
;;;;;;;;;;;;;;;;;;

(defun jt-org-roam-select-buffer ()
  "Select Org roam backlinks buffer, if it exists."
  (when (eq (org-roam-buffer--visibility) 'visible)
    (select-window (get-buffer-window org-roam-buffer))))

(defun jt-org-roam-at-node-p ()
  "Return non-nil if current heading appears to be an Org roam node."
  (save-excursion
    (save-restriction
      (org-narrow-to-subtree)
      (goto-char (point-min))
      (re-search-forward "^:ID:" nil t))))

(defun jt-org-roam-focus-tree (&rest _)
  "Go to current Org heading, reveal it and put it at top of screen.
If current heading is not an Org roam node, try to find one, and
ultimately go to beginning of buffer."
  (when (org-roam-file-p buffer-file-name)
    (if (org-before-first-heading-p)
        ;; This naturally overrides `save-place-mode', and I think
        ;; that's the right thing for org-roam
        (goto-char (point-min))
      (or (jt-org-roam-at-node-p) (re-search-backward "^\\* "))
      (if (not (jt-org-roam-at-node-p))
          (goto-char (point-min))
        (org-back-to-heading t)
        (recenter 0)
        (org-fold-show-entry)
        (org-with-limited-levels (org-fold-show-children))))
    ;; Hide property drawers when at beginning of buffer
    (and (bobp) (org-fold-hide-drawer-all))))


;;;;;;;;;;;;;;;;;;
;; For god-mode ;;
;;;;;;;;;;;;;;;;;;

(when jt--is-android
  (defun jt-god-mode-insert ()
    "Deactive god-mode. If region is active, delete it."
    (interactive)
    (and (region-active-p) (delete-region (point) (mark)))
    (god-local-mode -1)))


(provide 'jt-emacs-lisp)
