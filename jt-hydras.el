;;; jt-hydras.el --- Personal hydras for GNU Emacs  -*- lexical-binding: t -*-

;; Copyright (c) 2022-2023 John Task <q01@disroot.org>

;; Author: John Task <q01@disroot.org>
;; Version: 2.0.0
;; Package-Requires: ((emacs "28.1"))
;; URL: https://gitlab.com/q01_code/emacs-dotfiles

;; This file is not part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; My personal hydras for GNU Emacs.

;;; Code:

(defhydra hydra-kmacro (:hint nil :color pink :pre
                                (when defining-kbd-macro
                                    (kmacro-end-macro 1)))
    "
  ^Basic^        ^Edit^       ^Cycle^        ^Bind/Name^
╭────────────────────────────────────────────────╯
  _c_reate       _a_ny        _n_ext         _N_ame
  _e_xecute      _l_ast       _p_revious     _B_ind
  _r_egion       _s_tep                    _i_nsert
"
  ("c" kmacro-start-macro :color blue)
  ("r" apply-macro-to-region-lines)
  ("a" edit-kbd-macro)
  ("l" kmacro-edit-macro)
  ("e" kmacro-end-or-call-macro-repeat)
  ("s" kmacro-step-edit-macro)
  ("n" kmacro-cycle-ring-previous)
  ("p" kmacro-cycle-ring-next)
  ("N" kmacro-name-last-macro)
  ("B" kmacro-bind-to-key)
  ("i" insert-kbd-macro)
  ("q" nil :color blue))

(defhydra hydra-dired-command (:hint nil :color blue)
  "
  ^Shell^      ^File^      ^Mark^     ^Other^    ^Movement^
╭──────────────────────────────────────────────────────╯
  _a_sync      _R_ename    _r_egexp   _s_ort     _n_ext page
  _c_urrent    _C_opy      _t_oggle   r_g_rep    _p_revious page
  _e_at        _D_elete
             _S_ymlink
             _T_ouch
             com_P_ress
"
  ;; Shell
  ("SPC" async-shell-command)
  ("a" async-shell-command)
  ("c" dired-do-async-shell-command)
  ("e" eat)
  ;; File
  ("R" dired-do-rename)
  ("C" dired-do-copy)
  ("D" dired-do-delete)
  ("S" dired-do-symlink)
  ("T" dired-do-touch)
  ("P" dired-do-compress)
  ;; Mark
  ("r" dired-mark-files-regexp)
  ("t" dired-toggle-marks)
  ;; Other
  ("s" jt-dired-sort)
  ("g" rgrep)
  ;; Movement
  ("n" scroll-up-command :color red)
  ("p" scroll-down-command :color red))

(defhydra hydra-emms (:hint nil :color blue)
  "
  ^Play controls^      ^General^
╭────────────────────────────────────╯
  _P_ause              _m_ain
  _s_top               _d_isplay current
  volume (_+_, _-_)      _b_rowser
  _n_ext               _a_dd directory
  _p_revious
  _c_hoose song
  forward (_>_)
  backward (_<_)
"
  ("m" emms)
  ("+" emms-volume-raise :color red)
  ("-" emms-volume-lower :color red)
  ("P" emms-pause)
  ("s" emms-stop)
  ("n" emms-next :color red)
  ("p" emms-previous :color red)
  (">" emms-seek-forward :color red)
  ("<" emms-seek-backward :color red)
  ("d" emms-show)
  ("b" emms-browser)
  ("c" consult-emms-current-playlist)
  ("a" emms-add-directory-tree)
  ("q" nil))


(provide 'jt-hydras)
