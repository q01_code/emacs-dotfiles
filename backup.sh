#!/bin/bash

## Copyright (c) 2022-2023  John Task <q01@disroot.org>

## Author: John Task <q01@disroot.org>

## This file is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by the
## Free Software Foundation, either version 3 of the License, or (at
## your option) any later version.
##
## This file is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this file.  If not, see <http://www.gnu.org/licenses/>.

### Commentary:

## This is a bash script I use to backup and encrypt several files from
## my Emacs config.
##
## The reasons why I do so are because i) there's nothing useful there,
## and ii) they can contain sensitive information.

### Code:

# Clean old files
if [ -e packages.tar.gz.gpg ]
then
    rm packages.tar.gz.gpg
fi

if [ -e personal.tar.gz.gpg ]
then
    rm personal.tar.gz.gpg
fi

# Find any Elisp files and store them
find ./lisp-dir -name '*.el' -print0 | tar --create --null --files-from=- --file=./packages.tar
find ./personal-lisp-dir -name '*.el' -print0 | tar --create --null --files-from=- --file=./personal.tar

# Add other personal settings and files
tar -rf personal.tar personal.el
tar -rf personal.tar elfeed.score

# Compress them
gzip packages.tar
gzip personal.tar

# Encrypt them
gpg -c --passphrase-file password.txt --pinentry-mode loopback --batch packages.tar.gz
gpg -c --passphrase-file password.txt --pinentry-mode loopback --batch personal.tar.gz

# Clean
rm packages.tar.gz personal.tar.gz
