;;; init.el --- Personal configuration file -*- lexical-binding: t -*-

;; Copyright (c) 2022-2023 John Task <q01@disroot.org>

;; Author: John Task <q01@disroot.org>
;; Version: 2.0.0
;; Package-Requires: ((emacs "28.1"))
;; URL: https://gitlab.com/q01_code/emacs-dotfiles

;; This file is not part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; My personal configuration for GNU Emacs.

;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Initial configuration ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Set up my functions and macros
(let ((load-path (append (ensure-list user-emacs-directory) load-path)))
  ;; The load-path is set just for this function, as
  ;; 'user-emacs-directory' shouldn't be there
  (require 'jt-emacs-lisp))

;; Keep emacs Custom-settings in separate file, and load it now
(setq custom-file (locate-user-emacs-file "custom.el"))
(load custom-file)

;; Use a bar as cursor
(setopt cursor-type 'bar)

;; Show custom welcome message
(add-hook 'emacs-startup-hook #'jt-show-welcome-buffer)

;;;;;;;;;;;;;;;;;;;;;
;; Better defaults ;;
;;;;;;;;;;;;;;;;;;;;;

;; Sentences end with a single space
(setq sentence-end-double-space nil)

;; Use short aswers
(setq use-short-answers t)

;; Select help window
(setq help-window-select t)

;; Better backup handle
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups/")))
      version-control t
      vc-make-backup-files t
      backup-by-copying t
      delete-old-versions -1)

;; Write all autosave files in the tmp dir
(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;; Don't signal an error when killing at read-only buffers
(setq kill-read-only-ok t)

;; Use UTF-8 coding
(set-default-coding-systems 'utf-8)

;; Revert buffers automatically
(setq auto-revert-use-notify nil
      global-auto-revert-non-file-buffers t)
(global-auto-revert-mode)

;; Reduce verbosity
(setq auto-revert-verbose nil
      auto-save-no-message t)

;; Smart minibuffer
(minibuffer-electric-default-mode)

;; Display line numbers on programming modes
(unless jt--is-android
  (add-hook 'prog-mode-hook #'display-line-numbers-mode)
  (setq display-line-numbers-width 3))

;; Shorten default message in minibuffer
(setq minibuffer-default-prompt-format " [%s]")

;; Better scrolling
(setq scroll-error-top-bottom t
      scroll-conservatively 200
      scroll-preserve-screen-position 1
      redisplay-skip-fontification-on-input t)

;; Save minibuffer history
(setq history-delete-duplicates t)
(savehist-mode)

;; Enable recursive minibuffers
(setq enable-recursive-minibuffers t)
(minibuffer-depth-indicate-mode)

;; Uniquify buffer names
(setq uniquify-buffer-name-style 'forward)

;; Do not copy the same thing twice
(setq kill-do-not-save-duplicates t)

;; Better performance on long buffers
(setq-default bidi-paragraph-direction 'left-to-right)
(setq bidi-inhibit-bpa t)
(global-so-long-mode)

;; Save last place visited for every buffer
(save-place-mode)
(setq save-place-file (locate-user-emacs-file ".places"))

;; Don't use tabs for indentation
(setq-default indent-tabs-mode nil)

;; Behave as a pager when on read-only buffers
(setq view-read-only t)

;; Repeat simple commands easily
(repeat-mode)

;; Use C-SPC for popping mark
(setq set-mark-command-repeat-pop t)

;; Decrease mark ring limit
(setq mark-ring-max 6
      global-mark-ring-max 8)

;; Increase macro ring limit
(setq kmacro-ring-max 16)

;; Don't highlight links when mouse ends there by mistake
(setq mouse-highlight 1)

;; Preserve system clipboard's content
(setq save-interprogram-paste-before-kill t)

;; Don't ring
(setq ring-bell-function 'ignore)

;; Display register window inmediately
(setq register-preview-delay 0)

;; Allow undoing window changes
(winner-mode)

;; Replace selection when active
(delete-selection-mode)

;; Confirm when killing Emacs
(setq confirm-kill-emacs 'yes-or-no-p)

;;;;;;;;;;;;;;;;;;;;;;;
;; Setup use-package ;;
;;;;;;;;;;;;;;;;;;;;;;;

(unless (require 'use-package nil t)
  ;; use-package was not found; this is the case on Emacs <29.
  ;; Fortunately, I have it in my lisp-dir, too.
  (add-to-list 'load-path (concat jt-emacs-load-path "use-package"))
  (require 'use-package))

;; Here I add my personal :local keyword, defined in jt-emacs-lisp.
;;
;; Normally, one should put custom keywords at the end of the list.
;; However, it is important that the load path is added
;; before requiring the package; hence, it is put at the start.
(add-to-list 'use-package-keywords :local)

;;;;;;;;;;;;;;;;;;;;
;; Setup packages ;;
;;;;;;;;;;;;;;;;;;;;

(use-package abbrev
  :hook (org-mode text-mode markdown-mode)
  :config
  ;; Save abbrevs without asking
  (setq save-abbrevs 'silently))

(use-package ace-link
  :local t
  :bind ("M-<menu>" . ace-link))

(use-package all-the-icons
  :local t
  :defer t)

(use-package all-the-icons-dired
  :local t
  :if (display-graphic-p)
  :hook dired-mode)

(use-package avy
  :local t
  :bind (("C-ñ" . jt-avy-embark)
         :map isearch-mode-map
         ("<tab>" . avy-isearch))
  :commands avy-goto-char-2
  :config
  ;; Set avy specific keys
  ;;; Home row
  (setq avy-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l ?ñ))
  ;; Don't ignore case
  (setq avy-case-fold-search nil)
  ;; Setup custom functions
  (setf (alist-get ?n avy-dispatch-alist) 'jt-avy-action-forward-sexp))

(use-package bookmark
  :config
  ;; Don't show bookmark indicators
  (setq bookmark-fringe-mark nil)
  ;; Save bookmarks when adding or deleting them
  (setq bookmark-save-flag 1))

(use-package calc
  :bind (:map calc-mode-map
         ("M-o" . calc-other-window)))

(use-package calendar
  :hook
  ;; Mark current day
  (calendar-today-visible . calendar-mark-today)
  :config
  ;; Set monday as the starting day of the week
  (setq calendar-week-start-day 1)
  ;; Increase padding
  (setq calendar-left-margin 21)
  ;; Clean mode line
  (setq calendar-mode-line-format ""))

(use-package cape
  :local t
  :after corfu
  :autoload cape-dict)

(use-package capital
  ;; 'capital' is a personal modification of captain-mode.
  ;;
  ;; <https://gitlab.com/q01_code/capital>
  :local t
  :hook (org-mode text-mode markdown-mode))

(use-package cc-mode
  :bind (:map c-mode-map
         ("C-c C-SPC"      . recompile)
         ("C-c C-<return>" . compile))
  :hook
  ;; Insert newlines automatically
  (c-mode . c-toggle-auto-newline))

(use-package change-inner
  :local t
  :bind (("C-c k" . change-inner)
         ("C-c K" . change-outer)))

(use-package climber
  ;; 'climber' is a package I wrote for crafting a
  ;; Magit-like UI for any CLI program.
  ;;
  ;; <https://gitlab.com/q01_code/climber>
  :local t
  :config
  ;; Require my climbs
  (jt-with-emacs-home-in-path
    (require 'jt-climbs)))

(use-package consult
  :local t
  :bind (("M-s M-b"   .  consult-buffer)
         ("M-s M-f"   .  consult-find)
         ("M-s M-g"   .  consult-grep)
         ("M-s M-l"   .  consult-line)
         ("M-s M-m"   .  consult-mark)
         ("M-s M-s"   .  consult-outline))
  :hook
  (;; Put point at top after jump
   (consult-after-jump . jt-put-line-at-top)
   ;; And also reveal entry
   (consult-after-jump . jt-reveal-entry))
  :config
  ;; Don't preview inmediately
  (setq consult-preview-key (list :debounce 0.5 'any))
  ;; Increase waiting for asynchronous commands
  (setq consult-async-input-debounce 0.5)
  (setq consult-async-input-throttle 0.8)
  ;; Use as provider for completion-in-region
  (setq completion-in-region-function #'consult-completion-in-region)
  ;; Remove recenter from after jump hook
  (remove-hook 'consult-after-jump-hook #'recenter))

(use-package consult-dir
  :local t
  :bind (:map minibuffer-mode-map
         ("C-<menu>" . consult-dir))
  :config
  ;; Remove recentf from sources (it's slow)
  (setq consult-dir-sources
        '(consult-dir--source-bookmark
          consult-dir--source-default
          consult-dir--source-project
          consult-dir--source-tramp-local)))

(use-package consult-emms
  :local t
  :commands consult-emms-current-playlist)

(use-package consult-imenu
  :bind ("M-s M-i" . consult-imenu))

(use-package consult-register
  :bind ("M-s M-SPC" . consult-register))

(use-package corfu
  ;; NOTE: I don't enable corfu here; I prefer using
  ;; 'consult-completion-in-region' for that functionality.
  ;; I only use corfu for 'jt-writer-mode'; see jt-emacs-lisp.
  :local t
  :commands corfu-mode)

(use-package dired
  :bind (:map dired-mode-map
         ("c"   .  dired-do-async-shell-command)
         ("E"   .  jt-dired-ediff)
         ("f"   .  consult-find)
         ("F"   .  find-name-dired)
         ("r"   .  dired-mark-files-regexp)
         ("s"   .  isearch-forward)
         ("u"   .  dired-up-directory)
         ("SPC" .  hydra-dired-command/body))
  :hook
  (;; Hide details
   (dired-mode . dired-hide-details-mode)
   ;; Truncate lines
   (dired-mode . jt-truncate-lines))
  :config
  ;; Don't use more than one buffer
  (setq dired-kill-when-opening-new-dired-buffer t)
  ;; Use other window as default target
  (setq dired-dwim-target t)
  ;; Don't show free space
  (setq dired-free-space nil)
  ;; Create directories if I want to
  (setq dired-create-destination-dirs 'ask)
  ;; Move files to trash
  (setq delete-by-moving-to-trash t)
  ;; Revert directories at revisiting them, if they changed
  (setq dired-auto-revert-buffer #'dired-directory-changed-p)
  ;; Also revert buffer after operations
  (setq dired-do-revert-buffer t)
  ;; Show folders before files
  (setq dired-listing-switches "-al --group-directories-first")
  ;; Don't keep these marks after acting
  (setq dired-keep-marker-copy nil
        dired-keep-marker-rename nil)
  ;; Set default programs for async-shell-command
  (setq dired-guess-shell-alist-user
        `(("\\.png\\'" "gimp")
          ("\\.jpe?g\\'" "gimp")
          ("\\.mp[34]\\'" "mpv")
          ("\\.opus\\'" "mpv")
          ("\\.mkv\\'" "mpv")
          ("\\.webm\\'" "mpv"))))

(use-package doom-themes
  :local t
  :if (display-graphic-p))

(use-package eat
  :local t
  :bind ("C-c s t" . eat)
  :hook
  ;; Enable eat mode for terminal emulation
  (eshell-mode . eat-eshell-mode)
  :config
  ;; Disable key logging on eat buffers (sudo password, anyone?)
  (jt-setq-hook eat-mode-hook inhibit--record-char t)
  ;; Also hide mode line
  (jt-setq-hook eat-mode-hook mode-line-format nil))

(use-package ediff
  :bind ("C-c E" . ediff)
  :config
  ;; Use windows instead of frames
  (setq ediff-window-setup-function 'ediff-setup-windows-plain))

(use-package elfeed
  :local t
  :bind ("C-c e" . elfeed)
  :hook
  (;; Hide cursor
   (elfeed-show-mode . jt-hide-cursor-mode)
   ;; Hide mode line
   (elfeed-show-mode . jt-hide-mode-line)
   ;; Truncate lines
   (elfeed-search-mode . jt-truncate-lines))
  :config
  ;; Increase line spacing
  (jt-setq-hook elfeed-show-mode-hook line-spacing 2)
  ;; Compact date format
  (setq elfeed-search-date-format '("%m-%d" 5 :left)))

(use-package elfeed-score
  :local t
  :after elfeed
  :config
  ;; Enable score
  (elfeed-score-enable)
  ;; Bind to elfeed
  (keymap-set elfeed-search-mode-map "=" elfeed-score-map))

(use-package elpher
  :local t
  :commands elpher
  :config
  ;; Increase text width
  (setq elpher-gemini-max-fill-width 200)
  ;; Set Gemini as default scheme
  (setq elpher-default-url-type "gemini"))

(use-package emacsql
  :local t
  :defer t)

(use-package embark
  :local t
  :demand t
  :bind (("C-<return>" . embark-act)
         :map embark-general-map
         ("k" . kill-region)
         :map embark-become-file+buffer-map
         ("r" . recentf-open)
         :map minibuffer-mode-map
         ("C-M-<return>" . embark-become)
         ("C-S-<return>" . embark-export))
  :config
  ;; Prefer minimal indicator
  (setq embark-indicators
        '(embark--vertico-indicator
          embark-minimal-indicator
          embark-highlight-indicator
          embark-isearch-highlight-indicator)))

(use-package emms
  :local t
  :if (not jt--is-android)
  :commands (emms emms-volume-lower emms-volume-raise emms-browser)
  :bind (:map emms-playlist-mode-map
         ("<backspace>" . scroll-down-command)
         ("SPC"         . scroll-up-command)
         ("i"           . emms-insert-file))
  :config
  ;; Enable default settings
  (require 'emms-setup)
  (emms-all)
  ;; Use mpv as provider
  (setq emms-player-list '(emms-player-mpv))
  ;; Don't display song in mode line
  (emms-mode-line-mode -1)
  ;; Use GNU find for searching files
  (setq emms-source-file-directory-tree-function
        #'emms-source-file-directory-tree-find)
  ;; Don't load metadata asynchronously
  (setq emms-info-asynchronously nil)
  ;; Increase line spacing
  (jt-setq-hook emms-browser-mode-hook line-spacing 10)
  (jt-setq-hook emms-playlist-mode-hook line-spacing 10))

(use-package erc
  :commands (erc erc-tls)
  :config
  ;; Set basic information
  (setq erc-server "irc.libera.chat"
        erc-nick "q01"
        erc-user-full-name "John Task"
        erc-autojoin-channels-alist '(("irc.libera.chat" "#emacs"))))

(use-package eshell
  :bind ("C-c s e" . eshell)
  :config
  ;; Change prompt
  (setq eshell-prompt-regexp "└─[$#] ")
  (setq eshell-prompt-function #'jt-default-prompt-function)
  ;; Disable visual commands (eat takes care of it)
  (setq eshell-visual-commands nil)
  ;; Disable key logging
  (jt-setq-hook eshell-mode-hook inhibit--record-char t)
  ;; Hide mode line
  (jt-setq-hook eshell-mode-hook mode-line-format nil))

(use-package esxml
  :local t
  :defer t)

(use-package ett
  ;; 'ETT' is a package I wrote for tracking what I do
  ;; every day.
  ;; <https://gitlab.com/q01_code/ett>
  ;;
  ;; Note that most of my configuration is in personal.el
  ;; as it is not of much value to public.
  :local t
  :bind (("C-c f" . ett-find-file)
         ("C-c r" . ett-report)
         ("C-c R" . ett-report-item))
  :config
  ;; Use icons
  (and (display-graphic-p) (setq ett-use-icons t))
  ;; Show indicator in mode line
  (ett-modeline-mode))

(use-package eww
  :bind ("C-c W" . eww)
  :config
  ;; Increase line spacing
  (jt-setq-hook eww-mode-hook line-spacing 5))

(use-package expand-region
  :local t
  :bind (("C-+" . er/expand-region))
  :autoload er/mark-symbol)

(use-package f
  :local t
  :defer t)

(use-package gemini-mode
  :local t
  :mode "\\.gmi\\'")

(use-package gnuplot
  :local t
  :defer t)

(use-package god-mode
  ;; I don't like modal editing, but on Android I have no alternative
  :local t
  :if jt--is-android
  :bind (("&" . god-local-mode)
         :map god-local-mode-map
         ("." . repeat)
         ("i" . jt-god-mode-insert))
  :config
  ;; Set custom keys
  (setq god-mode-alist '((nil . "C-") ("m" . "M-") ("M" . "C-M-")))
  ;; Enable mode
  (god-mode))

(use-package good-line
  ;; 'good-line' is a personal modification of 'mood-line'.
  ;; I haven't published it because it doesn't improve
  ;; too much.
  :local t
  :config
  ;; Enable mode line
  (good-line-mode))

(use-package grep
  :bind ("M-s g" . grep)
  :config
  ;; Use headings
  (and (>= emacs-major-version 30) (setq grep-use-headings t))
  ;; Don't ask to save buffers
  (setq grep-save-buffers nil)
  ;; Set default command
  (setopt grep-command "grep --color=auto -nHir -e ")
  ;; Don't use null device
  (setopt grep-use-null-device nil))

(use-package highlight-numbers
  :local t
  :hook prog-mode)

(use-package hippie-exp
  :bind ("C-," . hippie-expand)
  :config
  ;; Custom list of functions
  (setq hippie-expand-try-functions-list
        '(try-expand-dabbrev
          try-complete-file-name-partially
          try-complete-file-name
          try-expand-all-abbrevs
          try-expand-list
          try-expand-line
          try-expand-dabbrev-all-buffers
          try-expand-dabbrev-from-kill
          try-complete-lisp-symbol-partially
          try-complete-lisp-symbol)))

(use-package hydra
  :local t
  :config
  ;; Load my hydras
  (jt-with-emacs-home-in-path
    (require 'jt-hydras)))

(use-package ibuffer
  :bind ("C-c b" . ibuffer)
  :hook
  ;; Truncate lines
  (ibuffer . jt-truncate-lines))

(use-package iedit
  :local t
  :bind ("C-c z" . iedit-mode))

(use-package imenu
  :hook
  ;; Reposition window
  (imenu-after-jump . reposition-window)
  :config
  ;; Don't replace space character
  (setq imenu-space-replacement " "))

(use-package isearch
  :bind (:map isearch-mode-map
         ("<backspace>" .  jt-search-isearch-abort-dwim)
         ("C-<return>"  .  jt-search-isearch-other-end)
         ("C-{"         .  isearch-beginning-of-buffer)
         ("C-}"         .  isearch-end-of-buffer)
         ("C-q"         .  isearch-query-replace-regexp))
  :config
  ;; Set up keys
  (when jt--is-android
    ;; These keys aren't really common, so it makes sense
    ;; to use them this way in mobile devices.
    (bind-keys
     :map isearch-mode-map
     (">" . isearch-repeat-forward)
     ("<" . isearch-repeat-backward)))
  ;; Main options
  (setq isearch-wrap-pause 'no-ding
        isearch-lazy-count t
        isearch-repeat-on-direction-change t
        isearch-allow-scroll t
        lazy-highlight-no-delay-length 2
        search-default-mode 'char-fold-to-regexp))

(use-package jinx
  :local t
  :bind ("C-." . jinx-correct)
  :autoload jinx-mode
  :config
  ;; Set delay
  (setq jinx-delay 0.7)
  ;; Set languages
  (setq jinx-languages "en es"))

(use-package lexic
  :local t
  :bind (("C-c w" . lexic-search)
         :map lexic-mode-map
         ("ñ" . jt-avy-define)
         ;; For consistence
         ("l" . lexic-search-history-backwards)
         ("r" . lexic-search-history-forwards)
         :map embark-identifier-map
         ("d" . lexic-search-word-at-point)
         ;; Fallback
         ("D" . lexic-search-word-at-point))
  :config
  ;; Increase timeout
  (setq lexic-wait-timeout 10))

(use-package magit
  :local "magit/lisp"
  :bind (("C-x g" . magit-status)
         ("C-c g" . magit-file-dispatch)))

(use-package marginalia
  :local t
  :if (not jt--is-android)
  :config
  ;; Enable mode
  (marginalia-mode))

(use-package markdown-mode
  :local t
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.\\(?:md\\|markdown\\|mdwn\\)\\'" .  markdown-mode))
  :config
  ;; Prettify bullets
  (font-lock-add-keywords
   'markdown-mode
   '(("^ *\\([\\*]\\) "
      (0
       (prog1 ()
         (compose-region (match-beginning 1) (match-end 1) "•")))))))

(use-package midnight
  :if (not jt--is-android)
  :config
  ;; Decrease time to wait before killing a buffer
  (setq clean-buffer-list-delay-general 2)
  ;; Kill buffers created by shell commands
  (setq clean-buffer-list-kill-regexps '("\\`\\*Man " "\\*Async Shell"))
  ;; Enable mode
  (midnight-mode))

(use-package minibuffer
  :config
  ;; Always showing completions buffer
  (setq completion-auto-help 'always)
  ;; Select completions buffer at second tab
  (setq completion-auto-select 'second-tab)
  ;; TAB completes things
  (setq tab-always-indent 'complete))

(use-package move-text
  :local t
  :config
  ;; Set up keys
  ;;; M-<up> and M-<down> by default
  (move-text-default-bindings))

(use-package mu4e
  :local t
  :if (not jt--is-android)
  :bind ("C-x m" . mu4e)
  :config
  ;; Set email retrieving command
  (setq mu4e-get-mail-command "mpop -q")
  ;; Set email sending function
  (setq send-mail-function 'smtpmail-send-it)
  ;; Increase legibility
  (setq shr-color-visible-luminance-min 60))

(use-package nov
  :local t
  :mode ("\\.epub\\'" . nov-mode)
  :hook (nov-post-html-render . jt-nov-post-html-render-hook)
  :bind (:map nov-mode-map
         ("<down>" .  jt-nov-forward-paragraph)
         ("<up>"   .  jt-nov-backward-paragraph)
         ("ñ"      .  jt-avy-define)
         ("w"      .  lexic-search))
  :config
  ;; Don't fill text
  (setq nov-text-width t))

(use-package orderless
  :local t
  :config
  ;; Basic setup
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion))))
  ;; Ignore case when using basic completion
  (setq read-file-name-completion-ignore-case t
        read-buffer-completion-ignore-case t
        completion-ignore-case t))

(use-package org
  :bind (("C-c a"           .  org-agenda)
         ("C-c C"           .  org-capture)
         ("C-c l"           .  org-store-link)
         :map org-mode-map
         ("C-c C-<return>"  .  org-babel-execute-buffer)
         ("C-c C-SPC C-b"   .  jt-org-make-bold)
         ("C-c C-SPC C-c"   .  jt-org-make-cursive)
         ("C-c C-SPC C-u"   .  jt-org-make-underline)
         ("C-c C-SPC C-SPC" .  jt-org-prettify-buffer)
         ("C-M-<return>"    .  org-insert-heading-respect-content)
         ("C-<return>"      .  nil)    ; unbind
         ("C-,"             .  nil)    ; unbind
         ("RET"             .  jt-org-return-dwim))
  :hook
  ;; Use TAB for returning to current heading
  (org-tab-before-tab-emulation . jt-org-back-to-heading)
  :config
  ;; Set up extra keys
  (when jt--is-android
    (bind-key "C-ñ" #'org-cycle org-mode-map))

  ;; Main settings
  (setq
   ;; Don't split lines
   org-M-RET-may-split-line nil
   ;; Prevent some invisible edits
   org-fold-catch-invisible-edits 'smart
   ;; Indent buffers
   org-startup-indented t
   ;; Use speed keys
   org-use-speed-commands t
   ;; RET follows links
   org-return-follows-link t
   ;; Special CTRL-A/E
   org-special-ctrl-a 'reversed
   ;; Cycle all when at beginning of buffer
   org-cycle-global-at-bob t
   ;; More imenu cover
   org-imenu-depth 7
   ;; Never show empty lines
   org-cycle-separator-lines 0
   ;; Fontify all blocks
   org-fontify-quote-and-verse-blocks t
   ;; Don't add extra indentation
   org-src-preserve-indentation t
   ;; Replace the three dots with a plus sign
   org-ellipsis " +")

  ;; Task management settings
  (setq
   ;; Set TODO state without displaying a new window
   org-use-fast-todo-selection 'expert
   ;; Log into a drawer
   org-log-into-drawer t
   ;; Don't separate tags from tasks
   org-tags-column 0
   ;; Delete other windows when entering agenda
   org-agenda-window-setup 'only-window
   ;; Use sticky agenda
   org-agenda-sticky t
   ;; Don't delete entries by accident
   org-agenda-confirm-kill t
   ;; Don't show clocked item on mode line (distracting)
   org-clock-clocked-in-display nil)

  ;; Change how frame is set up after opening a link
  (setq org-link-frame-setup
        '((vm . vm-visit-folder-other-frame)
          (vm-imap . vm-visit-imap-folder-other-frame)
          (gnus . org-gnus-no-new-news)
          (file . find-file)
          (wl . wl-other-frame)))

  ;; Set up Org Babel
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((C . t)    (python . t)  (shell . t)
     (calc . t) (gnuplot . t) (scheme . t)
     (emacs-lisp . t))))

(use-package org-clock
  :autoload org-clocking-p)

(use-package org-habit
  :after org)

(use-package org-protocol
  :after org)

(use-package org-roam
  :local t
  :bind (("C-c n g"  .  org-id-get-create)
         ("C-c n i"  .  org-roam-node-insert)
         ("C-c n l"  .  org-roam-buffer-toggle)
         ("C-c n n"  .  org-roam-node-find)
         ("C-c n s"  .  jt-consult-roam))
  :hook
  ;; Focus tree after following a link
  (org-follow-link-hook . jt-org-roam-focus-tree)
  :config
  ;; NOTE: The call to `org-roam-db-autosync-mode' is done at personal.el
  ;;
  ;; Complete links everywhere
  (setq org-roam-completion-everywhere t)
  ;; Focus tree after opening a node
  (advice-add 'org-roam-node-visit :after #'jt-org-roam-focus-tree)
  ;; Always select backlinks buffer
  (advice-add 'org-roam-buffer-toggle :after #'jt-org-roam-select-buffer))

(use-package org-roam-graph
  :local "org-roam/extensions"
  :bind ("C-c n r" . org-roam-graph)
  :config
  ;; View graphs inside Emacs
  (setq org-roam-graph-viewer nil)
  ;; Hide all links on graphs, except those pointing to nodes
  (setq org-roam-graph-link-hidden-types
        '("file" "http" "https" "info" "gemini")))

(use-package org-roam-ui
  :local t
  :bind ("C-c n u" . org-roam-ui-mode))

(use-package org-tempo
  :after org)

(use-package osm
  :local t
  :if (display-graphic-p)
  :autoload osm-bookmark-jump
  :hook
  ;; Truncate lines
  (osm-mode . jt-truncate-lines))

(use-package ox-extra
  :local "org-contrib/lisp"
  :after org
  :config
  ;; Ignore headings with a "ignore" tag, but not its content.
  (ox-extras-activate '(ignore-headlines)))

(use-package ox-md
  :after org)

(use-package ox-texinfo
  :after org)

(use-package parent-mode
  :local t
  :defer t)

(use-package pdf-tools
  :local "pdf-tools/lisp"
  :if (display-graphic-p)
  :magic ("%PDF" . pdf-view-mode)
  :config
  ;; Require statements
  ;;; I'm not sure why pdf-tools-install-noverify fails
  ;;; if this is not here...  But it doesn't hurt, anyway
  (require 'pdf-annot)
  (require 'pdf-history)
  (require 'pdf-links)
  (require 'pdf-occur)
  (require 'pdf-outline)
  (require 'pdf-sync)
  ;; Install
  (pdf-tools-install-noverify))

(use-package proced
  :bind ("C-c x p" . proced)
  :config
  ;; Enable colors
  (setq proced-enable-color-flag t))

(use-package recentf
  :demand t
  :bind ("C-x f" . recentf-open)
  :config
  ;; Enable mode
  (recentf-mode)
  ;; Increase limit
  (setq recentf-max-saved-items 750))

(use-package replace
  :hook
  ;; Always select occur window
  (occur . jt-simple-other-window))

(use-package s
  :local t
  :defer t)

(use-package skeleton
  :bind (("«" . skeleton-pair-insert-maybe)
         ("¿" . skeleton-pair-insert-maybe)
         ("¡" . skeleton-pair-insert-maybe)
         ;; Prevent inserting pairs on minibuffer
         :map minibuffer-local-map
         ("«" . self-insert-command)
         ("¿" . self-insert-command)
         ("¡" . self-insert-command))
  :config
  ;; Set up pairs alist
  (setq skeleton-pair-alist
        '((?\¿ _ ?\?) (?\?)
          (?\¡ _ ?\!) (?\!)
	  (?« _ ?»)   (?\»)))
  ;; Enable pairing
  (setq skeleton-pair t))

(use-package server
  :config
  ;; Start the editing server
  (when (not (server-running-p))
    (server-start)))

(use-package simple-httpd
  :local t
  :commands (httpd-start httpd-serve-directory)
  :config
  ;; Set custom port
  (setq httpd-port 8090))

(use-package tablist
  :local t
  :defer t)

(use-package tempel
  :local t
  :bind ("C-c t" . tempel-insert))

(use-package transient
  :config
  ;; Use 'q' for quiting transient
  (transient-bind-q-to-quit))

(use-package which-key
  :local t
  :demand t
  :bind ("C-c h" . which-key-show-major-mode)
  :config
  ;; Main settings
  (setq which-key-sort-order #'which-key-key-order-alpha
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 6
        which-key-idle-delay 1.5
        which-key-side-window-slot -10)
  ;; Enable mode
  (which-key-mode))

(use-package vertico
  :local t
  :demand t
  :hook (minibuffer-setup . cursor-intangible-mode)
  :config
  ;; Enable mode
  (vertico-mode)
  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  ;; Show less candidates
  (setq vertico-count (if jt--is-android 5 8))
  ;; Enable cycling for `vertico-next' and `vertico-previous'.
  (setq vertico-cycle t))

(use-package vertico-directory
  :local "vertico/extensions"
  :after vertico
  :bind (:map vertico-map
         ("RET"     .  vertico-directory-enter)
         ("DEL"     .  vertico-directory-delete-char)
         ("M-DEL"   .  vertico-directory-delete-word)
         ("<right>" .  vertico-next)
         ("<left>"  .  vertico-previous)
         ("C-j"     .  vertico-exit-input)
         ("C-c C-c" .  jt-vertico-go-to-home))
  :hook
  ;; Hide directory when overwritten
  (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package wgrep
  :local t
  :after grep
  :bind (:map grep-mode-map
         ;; I never remember C-c C-p
         ("C-x C-q" . wgrep-change-to-wgrep-mode)))

(use-package wikipedia-mode
  :local t
  :mode "\\.wiki\\'")

(use-package xkcd
  :local t
  :bind ("C-c i x" . xkcd)
  :hook
  ;; Hide cursor
  (xkcd-mode . jt-hide-cursor-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom key bindings ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

(bind-keys
 :map global-map

 ;; These keys aren't defined by default, so I use them
 ("C-{"               .   beginning-of-buffer)
 ("C-}"               .   end-of-buffer)
 ("C-;"               .   comment-line)
 ("C-<tab>"           .   indent-region)
 ("C-x x a"           .   append-to-buffer)
 ("C-x x b"           .   jt-new-empty-buffer)
 ("C-x C-<backspace>" .   delete-region)
 ("M-n"               .   forward-paragraph)
 ("M-o"               .   other-window)
 ("M-p"               .   backward-paragraph)
 ("M-s M-h"           .   how-many)
 ("<f7>"              .   jt-hide-cursor-mode)
 ("<f8>"              .   jt-reader-mode)
 ("<f9>"              .   jt-writer-mode)

 ;; These are key bindings which I override for suiting my personal needs
 ("C-M-SPC"          .    jt-mark-dwim)                 ; was mark-sexp
 ("C-h h"            .    shortdoc)                     ; was view-hello-file
 ("C-o"              .    jt-smart-open-line)           ; was open-line
 ("C-q"              .    query-replace-regexp)         ; was quoted-insert
 ("C-z"              .    undo)                         ; was suspend-frame
 ("C-x k"            .    jt-kill-current-buffer)       ; was kill-buffer
 ("C-x C-b"          .    switch-to-buffer)             ; was list-buffers
 ("C-x C-d"          .    duplicate-dwim)               ; was list-directory
 ("C-x C-p"          .    kill-paragraph)               ; was mark-page
 ("C-x C-z"          .    undo)                         ; was suspend-frame
 ("M-c"              .    capitalize-dwim)              ; was capitalize-word
 ("M-i"              .    jt-activate-mark-interactive) ; was tab-to-tab-stop
 ("M-j"              .    delete-indentation)           ; was default-indent-new-line
 ("M-l"              .    downcase-dwim)                ; was downcase-word
 ("M-u"              .    upcase-dwim)                  ; was upcase-word
 ("M-z"              .    zap-up-to-char)               ; was zap-to-char
 ("M-;"              .    comment-region)               ; was comment-dwim

 ;; Shell related
 ("C-c s a"          .    async-shell-command)
 ("C-c s r"          .    shell-command-on-region)
 ("C-c s s"          .    shell-command)

 ;; System related
 ("C-c x b"          .    battery)
 ("C-c x c"          .    calendar)

 ;; File related
 ("C-c i i"          .    jt-open-init-file)
 ("C-c i s"          .    scratch-buffer)

 ;; Whitespace related
 ("C-c o o"          .    open-line)
 ("C-c o SPC"        .    jt-clean-whitespace-region)

 ;; Other
 ("C-c A"            .    align-regexp)
 ("C-c c"            .    hydra-kmacro/body)
 ("C-c m"            .    hydra-emms/body)
 ("C-c O"            .    jt-open-with)
 ("C-c p"            .    jt-increment-number-at-point)
 ("C-c q"            .    quoted-insert)
 ("C-c S"            .    sort-lines)
 ("C-c T"            .    transpose-regions)
 ("C-c u"            .    jt-copy-url-at-point)
 ("C-c v"            .    eval-region))

;; Android specific
(when jt--is-android
  (bind-keys
   :map global-map
   ("C-x C-1" . delete-other-windows)
   ("C-x C-0" . delete-window)))

;; Programming specific
(add-hook 'prog-mode-hook
          #'(lambda ()
              (keymap-local-set "M-RET" #'jt-open-line-and-move)))

;;;;;;;;;;;;;;;;;;
;; Miscellanous ;;
;;;;;;;;;;;;;;;;;;

;; Load my theme
(if (display-graphic-p)
    (load-theme 'doom-one t)
  (load-theme 'modus-vivendi t))

;; Don't write lock-files
(setq create-lockfiles nil)

;; Echo keystrokes inmediatly
(setq echo-keystrokes 0.02)

;; Display column number at the mode line
(column-number-mode)

;; Prevent selecting with shift
(setq shift-select-mode nil)

;; Disable cursor blinking
(blink-cursor-mode -1)

;; I think I know where I set the mark...
(transient-mark-mode -1)     ; this is Evil level of controversial! ;D

;; Wrap words using a custom mode
(jt-global-word-wrap-mode)

;; Don't use quotes on Help buffer
(setq help-clean-buttons t)

;; Enable those commands
(dolist (c '(narrow-to-region upcase-region downcase-region set-goal-column))
  (put c 'disabled nil))

;; Don't ask before using a new buffer for shell command
(setq async-shell-command-buffer 'new-buffer)

;; Use hl-line-mode on programming modes
(unless jt--is-android (add-hook 'prog-mode-hook #'hl-line-mode))

;; Don't convert uppercase keystrokes to downcase
(setq translate-upper-case-key-bindings nil)

;; Display battery at mode line
(unless jt--is-android
  (setq battery-update-interval 30)
  (display-battery-mode))

;; Temporarily highlight lines on some operations
(dolist (command '(recenter-top-bottom other-window))
  (advice-add command :after #'jt-pulse-line))

;; Create a repeat map for C-z
(defvar-keymap my-undo-repeat-map
  :repeat t
  "z" #'undo
  "Z" #'undo-redo)

;; Create a repeat map for repeating searches
(defvar-keymap isearch-repeat-map
  :repeat t
  "s" #'isearch-repeat-forward
  "r" #'isearch-repeat-backward)

;; Create a repeat map for scrolling other window
(defvar-keymap scroll-other-window-repeat-map
  :repeat t
  "v" #'scroll-other-window
  "V" #'scroll-other-window-down)

;; Show parens context using an overlay
(setq show-paren-context-when-offscreen 'overlay)

;; Indicate buffer boundaries
(setq-default indicate-buffer-boundaries '((top) (bottom . left)))

;; Extend fringes
(when (display-graphic-p)
  (set-fringe-mode 21))

;; Prevent region deletion with return
(put 'newline 'delete-selection nil)

;; Put region at new copy after duplicate-dwim
(when (boundp 'duplicate-region-final-position)
  (setq duplicate-region-final-position 1))

;; Show examples on *Help* buffer
(when (fboundp #'shortdoc-help-fns-examples-function)
  (add-hook
   'help-fns-describe-function-functions
   #'shortdoc-help-fns-examples-function))

;; Set custom display buffer settings
;; https://www.masteringemacs.org/article/demystifying-emacs-window-manager
(setq switch-to-buffer-obey-display-actions t
      display-buffer-alist
      '(;; Calendar at bottom
        ("\\*Calendar\\*" display-buffer-at-bottom
         (inhibit-same-window . t))
        ;; OSM at full screen
        ("\\*osm:.*\\*" display-buffer-full-frame)
        ;; Proced at same window
        ("\\*Proced\\*" display-buffer-same-window)
        ;; grep at same window
        ("\\*grep\\*" display-buffer-same-window)
        ;; Man at full screen
        ("\\*Man.*\\*" display-buffer-full-frame)
        ;; lexic at full screen
        ("\\*lexic\\*" display-buffer-full-frame)))

;; Load my personal settings
(load (locate-user-emacs-file "personal.el") t)

;;; init.el ends here
