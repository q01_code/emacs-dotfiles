#+title: My Emacs dotfiles
#+author: John Task

This repository contains my personal configuration for GNU Emacs. 

*Warning*: While you /can/ use any code you want from here, make sure you understand it before integrating it into your config!

* Details

This repository contains the following files:

- README.org :: This file.
- early-init.el :: This is the code that is run before anything else, immediately after Emacs starts booting.  Here I set frame parameters, adjust the GC consing temporarily, disable some UI elements, and so on.
- init.el :: This is the main configuration file, where I set most options and configure my packages.
- jt-emacs-lisp.el :: Here I store Elisp code which I extensively use on Emacs, for various purposes.  Most of the snippets are not written by me.
- jt-hydras.el :: Here I store my personal [[https://github.com/abo-abo/hydra][hydras]]. 
- jt-climbs.el :: Here I store my personal [[https://gitlab.com/q01_code/climber][climbs]].
- packages.tar.gz.gpg :: This encrypted file contains all my packages.  This way I can use Emacs from any machine where I clone this repository, without any additional download.
- personal.tar.gz.gpg :: This encrypted file contains personal or sensitive configurations.
- backup.sh :: A shell script for producing packages.tar.gz.gpg and personal.tar.gz.gpg, so its management becomes transparent and my setup can be replicated by anyone.
- restore.sh :: A shell script for restoring the contents of packages.tar.gz.gpg and personal.tar.gz.gpg.
- COPYING :: The GNU General Public License, under which I release all the code here.

Note that I don't use a package manager.  Instead of it, I clone all my packages manually and put them under a specific directory, in =~/.emacs.d/lisp-dir=.

* Requirements

| Software                       | Required by             |
|--------------------------------+-------------------------|
| GNU utils (grep, findutils...) | Various                 |
| curl                           | elfeed, osm             |
| epdinfo                        | pdf-tools               |
| git                            | magit                   |
| hunspell                       | jinx                    |
| libenchant                     | jinx                    |
| libpoppler                     | pdf-tools               |
| mpop                           | mu4e                    |
| mpv                            | emms                    |
| mu                             | mu4e                    |
| sdcv                           | lexic                   |
| sqlite3                        | org-roam                |
| texlive                        | org-mode (latex export) |
