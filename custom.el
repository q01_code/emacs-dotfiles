;; -*- coding: utf-8; lexical-binding: t; -*-

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "JetBrains Mono" :weight regular :height 145 :width normal))))
 '(denote-faces-link ((t (:inherit link :underline nil))))
 '(embark-target ((t (:inherit vertico-current))))
 '(markdown-list-face ((t (:foreground "dark turquoise"))))
 '(org-agenda-structure ((t (:foreground "#81d4fa" :weight bold :height 1.15 :family "Noto Sans"))))
 '(org-habit-alert-face ((t nil)))
 '(org-habit-clear-face ((t (:foreground "forestgreen"))))
 '(org-habit-clear-future-face ((t nil)))
 '(org-habit-ready-face ((t (:foreground "forestgreen"))))
 '(shr-mark ((t nil)))
 '(variable-pitch ((t (:height 1.15 :family "DejaVu Serif")))))
