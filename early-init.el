;;; early-init.el --- Personal early configuration file -*- lexical-binding: t -*-

;; Copyright (c) 2022-2023 John Task <q01@disroot.org>

;; Author: John Task <q01@disroot.org>
;; Version: 2.0.0
;; Package-Requires: ((emacs "28.1"))
;; URL: https://gitlab.com/q01_code/emacs-dotfiles

;; This file is not part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; My early personal configuration file for GNU Emacs.
;;
;; Code here is evaluated very early in startup time, so we should put
;; only what we really need.

;;; Code:

;;;;;;;;;;;;;;;;;;
;; Main options ;;
;;;;;;;;;;;;;;;;;;

(let ((file-name-handler-original file-name-handler-alist)
      ;; Eli Zaretskii has explained plenty of times we shouldn't
      ;; set this variable too high; so, I increase it just a little,
      ;; from 800kb to 1MB
      (normal-gc-cons-threshold 1000000)
      ;; However, we can do whatever we want while Emacs is starting!
      (init-gc-cons-threshold most-positive-fixnum))

  ;; Basic switches
  (setq package-enable-at-startup    nil                    ; We don't do that here.
        file-name-handler-alist      nil                    ; Reduce startup time.
        gc-cons-threshold            init-gc-cons-threshold ; Same as above.
        frame-inhibit-implied-resize t                      ; Same as above.
        load-prefer-newer            t                      ; Prefer newer code.
        inhibit-startup-message      t)                     ; No startup message.
  
  ;; Adjust garbage collection thresholds and file-name-handler after startup
  (defun jt-normalize-emacs-variables ()
    "Normalize Emacs settings after startup."
    (setq gc-cons-threshold        normal-gc-cons-threshold
          file-name-handler-alist  file-name-handler-original))
  (add-hook 'emacs-startup-hook #'jt-normalize-emacs-variables))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Adjust minibuffer garbage collection ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Create functions
(defun my-minibuffer-setup-hook ()
  (setq gc-cons-threshold most-positive-fixnum))

(defun my-minibuffer-exit-hook ()
  (setq gc-cons-threshold 1000000))

;; Add hooks
(add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook  #'my-minibuffer-exit-hook)

;;;;;;;;;;;;
;; Visual ;;
;;;;;;;;;;;;

;; Turn off UI
(menu-bar-mode -1)
(when (featurep 'scroll-bar)
  ;; Check because it fails in emacs-nox
  (scroll-bar-mode -1)
  (tool-bar-mode -1))

;; Fullscreen
(add-to-list 'default-frame-alist '(fullscreen . maximized))
